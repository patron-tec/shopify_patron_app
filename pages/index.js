import { Heading, Page } from "@shopify/polaris";
import Home from "../src/components/home";

export default function Index() {
  return (
    <Page>
      <Heading>
        Hey Vikas
        <span role="img" aria-label="tada emoji">
          🎉
        </span>
      </Heading>
      <Home />
    </Page>
  );
}
