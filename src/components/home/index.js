import React, { useState } from "react";
import { Query } from "react-apollo";

import styles from "./styles.module.scss";
import { GET_STORE_DATA } from "../../graphqlQueries/storeData";
import { MenuItem, Select } from "@mui/material";

const Home = () => {
  const [location, setLocation] = useState("");
  const handleChange = (event) => {
    setLocation(event.target.value);
  };
  return (
    <Query query={GET_STORE_DATA}>
      {({ data, loading, error }) => {
        if (loading) return <div>Loading...</div>;
        if (error) return <div>{error.message}</div>;
        return (
          <div className={styles.main}>
            <div className={styles.location}>
              <div className={styles.location_title}>
                <span>Please select a location: </span>
              </div>
              <div className={styles.dropDown}>
                <Select
                  sx={{ minWidth: 200, fontSize: "16px" }}
                  value={location?.name}
                  onChange={handleChange}
                >
                  {data.locations.edges.map((item, index) => {
                    return (
                      <MenuItem
                        sx={{ fontSize: "16px" }}
                        value={item.node}
                        key={index}
                      >
                        {item.node.name}
                      </MenuItem>
                    );
                  })}
                </Select>
              </div>
            </div>

            {location != "" && (
              <div className={styles.iframe}>
                <iframe
                  frameBorder={0}
                  style={{ width: "80vw", height: "100vh" }}
                  src={`https://app2.patrontec.com/stores?lat=${location?.address?.latitude}&lon=${location?.address?.longitude}&graphs=9699690`}
                />
              </div>
            )}
          </div>
        );
      }}
    </Query>
  );
};

export default Home;
