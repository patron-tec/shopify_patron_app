import { gql } from "apollo-boost";

export const GET_STORE_DATA = gql`
  query {
    locations(first: 3) {
      edges {
        node {
          name
          address {
            latitude
            longitude
          }
        }
      }
    }
  }
`;
